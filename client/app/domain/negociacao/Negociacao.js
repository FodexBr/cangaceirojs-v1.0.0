'use strict';

class Negociacao {
    
    constructor(args) {       
        let keys = Object.keys(args);

        keys.forEach((key, index) => {
            if(!key.startsWith("_")) 
                throw new Error(`A prop "${key}" deve começar com "_"(underline)`);

            if(args[key] instanceof Date) 
                args[key] = new Date(args[key]);
        })

        Object.assign(this, args);
        Object.freeze(this);
    }


    get data() {
      return new Date(this._data.getTime());
    }


    get quantidade() {
        return this._quantidade;
    }

    get valor() {
        return this._valor;
    }

    get volume() {
        return this._quantidade * this._valor;
    }



}

