class NegociacaoController {
    constructor() {

        const self = this;
        const $ = document.querySelector.bind(document);

        this._inputData = $('#data');
        this._inputQuantidade = $('#quantidade');
        this._inputValor = $('#valor');

        // this._negociacoes = new Negociacoes();
        this._negociacoes = ProxyFactory
            .create(
                new Negociacoes(),
                ['adiciona', 'esvazia'], 
                model => this._negociacoesView.update(model));
        this._negociacoesView = new NegociacoesView($("#negociacoes"));
        this._negociacoesView.update(this._negociacoes);

        this._mensagem = ProxyFactory
            .create(
                new Mensagem(), 
                ['texto'],
                model => this._mensagemView.update(model));
        this._mensagemView = new MensagemView($("#mensagemView"));
        this._mensagemView.update(this._mensagem);
    }


    adiciona(event) {
        event.preventDefault();

        this._negociacoes.adiciona(this._criaNegociacao());
        this._mensagem.texto = 'Negociação adicionada com sucesso!';

        this._limpar(event.target);
    }

    apaga() {
        this._negociacoes.esvazia();
        this._mensagem.texto = 'As negociações foram apagadas com sucesso!';
    }

    _criaNegociacao() {
        let $ = document.querySelector.bind(document);

        console.log($('#data').value);
        return new Negociacao({
            "_data": DateConverter.paraData($('#data').value),
            "_quantidade" : parseInt($('#quantidade').value),
            "_valor": parseFloat($('#valor').value)
        });
    }

    _limpar(target) {
        target.reset();
        this._inputQuantidade = 1;
        this._inputValor = 0.0;
        this._inputData.focus();
    }
}